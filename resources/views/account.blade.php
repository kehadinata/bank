<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('headerLogin')
    <body>
        <div class="flex-center position-ref full-height">
            @include('menuLogin',[
                'title' => 'Account'
            ])
            <div class="content">
                @if ($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                @endif
                <form action="{{ route('createAccount') }}" method="post">
                    @csrf
                    <label for="account_no">Nomor Rekening :</label>
                    <input type="text" required name="account_no">

                    <label for="pin">Pin :</label>
                    <input type="password" required name="pin">

                    <label for="pin">Konfirmasi Pin :</label>
                    <input type="password" required name="pin_confirmation">

                    <label for="saldo">Saldo Awal :</label>
                    <input type="number" required min="500000" name="saldo">

                    <input type="submit" value="Buat Akun Baru">
                </form>

                <table class="center" style="border: 1px solid black; margin-top:50px;">
                    <thead>
                        <tr>
                            <th style="border: 1px solid black">
                                Nomor Rekening
                            </th>
                            <th style="border: 1px solid black">
                                Saldo
                            </th>
                            <th style="border: 1px solid black">
                                Action
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($accounts as $account)
                            <tr>
                                <td style="border: 1px solid black">
                                    {{ $account->account_no }}
                                </td>
                                <td style="border: 1px solid black">
                                    Rp. {{ $account->saldo }}
                                </td>
                                <td style="border: 1px solid black">
                                    <a href="{{ route('editAccountPage',['account' => $account->id]) }}">Ubah</a>
                                    <a href="{{ route('editAccountPinPage',['account' => $account->id]) }}">Ganti PIN</a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
