<div class="links">
    <a href="{{ route('accountPage') }}">Akun</a>
    <a href="{{ route('depositPage') }}">Deposit</a>
    <a href="{{ route('withdrawPage') }}">Tarik Tunai</a>
    <a href="{{ route('transferPage') }}">Transfer</a>
</div>

<div class="title m-b-md">
    {{ $title }}
</div>
