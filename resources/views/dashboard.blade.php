<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('headerLogin')
    <body>
        <div class="flex-center position-ref full-height">
            @include('menuLogin', [
                'title' => 'Dashboard'
            ])

            <div class="content">
                <p>Username : {{ $user->username }} </p>
                <p>Nama Lengkap : {{ $user->name }} </p>
                <p>Jenis Kelamin : {{ $user->jenis_kelamin }}</p>
                <p>Hobby : </p>
                <p>
                    {{-- <ul>
                        @foreach ($user->hobbies as $hobby)
                            <li>
                                User Id : {{ $hobby->user_id }}, Hobby Name : {{ $hobby->hobby_name }}
                            </li>
                        @endforeach
                    </ul> --}}

                    <table class="center" style="border: 1px solid black">
                        <thead>
                            <tr>
                                <th style="border: 1px solid black">
                                    Nama Lengkap
                                </th>
                                <th colspan="2" style="border: 1px solid black">
                                    Nama Hobi
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($user->hobbies as $hobby)
                                <tr>
                                    <td style="border: 1px solid black"> {{ $user->name }} </td>
                                    <td style="border: 1px solid black"> {{ $hobby->hobby_name }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </p>


                <a href="{{ route('logout') }}"><button>LOGOUT</button></a>

            </div>
        </div>
    </body>
</html>
