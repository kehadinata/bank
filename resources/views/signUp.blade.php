<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Sign Up Bank
                </div>
                @if ($errors->any())
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                @endif
                <form method="POST" action="{{ route('signUpSubmit') }}">
                    @csrf
                    <div style="margin-bottom: 20px;">
                        <label for="username">Username</label>
                        <input required type="text" name="username">
                    </div>
                    <div style="margin-bottom: 20px;">
                        <label for="password">Password</label>
                        <input required type="password" name="password">
                    </div>
                    <div style="margin-bottom: 20px;">
                        <label for="password">Confirm Password</label>
                        <input required type="password" name="password_confirmation">
                    </div>
                    <div style="margin-bottom: 20px;">
                        <label for="name">Nama Lengkap</label>
                        <input required type="text" name="name">
                    </div>
                    <div style="margin-bottom: 20px;">
                        <label for="email">Email</label>
                        <input type="email" name="email">
                    </div>
                    <div style="margin-bottom: 20px;">
                        <label for="age">Umur</label>
                        <input type="number" name="umur">
                    </div>
                    <div style="margin-bottom: 20px;">
                        <input type="radio" id="lakiLaki" value="male" name="jenis_kelamin">
                        <label for="lakiLaki">Laki-Laki</label>
                        <input type="radio" id="perempuan" value="female" name="jenis_kelamin">
                        <label for="perempuan">Perempuan</label>
                    </div>
                    <div style="margin-bottom: 20px;">
                        <input type="checkbox" id="swimming" value="swimming" name="hobby[]">
                        <label for="lakiLaki">Berenang</label>
                        <input type="checkbox" id="reading" value="reading" name="hobby[]">
                        <label for="perempuan">Membaca</label>
                        <input type="checkbox" id="playingFootbal" value="playing-footbal" name="hobby[]">
                        <label for="perempuan">Bermain Bola</label>
                    </div>
                    <div style="margin-bottom: 20px;">
                        <label for="file">File KTP</label>
                        <input type="file" name="file_ktp">
                    </div>
                    <div>
                        <input type="submit" name="Sign Up" value="Sign Up">
                    </div>
                </form>

            </div>
        </div>
    </body>
</html>
