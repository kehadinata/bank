<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function accountPage()
    {
        $accounts = Account::where('user_id',auth()->user()->id)->get();
        return view('account',[
            'accounts' => $accounts
        ]);
    }

    public function createAccount(Request $request)
    {
        $request->validate([
            'account_no' => 'required',
            'pin' => 'confirmed',
            'saldo' => 'required|numeric|min:500000'
        ]);
        $requestData = $request->all();

        Account::create([
            'user_id' => auth()->user()->id,
            'account_no' => $requestData['account_no'],
            'pin' => Hash::make($requestData['pin']),
            'saldo' => $requestData['saldo']
        ]);

        return redirect()->route('accountPage');
    }
}
