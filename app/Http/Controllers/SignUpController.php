<?php

namespace App\Http\Controllers;

use App\Hobby;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SignUpController extends Controller
{
    public function signUpPage()
    {
        return view('signUp');
    }

    public function signUp(Request $request)
    {
        $request->validate([
            'password' => [
                'confirmed'
            ],
            'username' => 'required',
            'email' => 'required',
            'umur' => 'required'
        ]);
        $requestData = $request->all();

        $user = User::create([
            'username' => $requestData['username'],
            'password' => Hash::make($requestData['password']),
            'name' => $requestData['name'],
            'email' => $requestData['email'],
            'umur' => $requestData['umur'],
            'jenis_kelamin' => $requestData['jenis_kelamin'],
            'file_ktp' => $requestData['file_ktp'],
        ]);

        foreach ($requestData['hobby'] as $hobby) {
            Hobby::create([
                'user_id' => $user->id,
                'hobby_name' => $hobby
            ]);
        }

        return redirect()->route('loginPageView');
    }
}
