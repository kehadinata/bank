<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login','LoginController@loginPage')->name('loginPageView');
Route::post('/login','LoginController@login')->name('loginSubmit');

Route::get('/sign-up','SignUpController@signUpPage')->name('signUpPageView');
Route::post('/sign-up','SignUpController@signUp')->name('signUpSubmit');

Route::middleware('auth')->group(function() {
    Route::get('dashboard','DashboardController@dashboardPage')->name('dashboard');

    Route::get('account','AccountController@accountPage')->name('accountPage');
    Route::post('account','AccountController@createAccount')->name('createAccount');
    Route::get('account/{account}', 'AccountController@editAccountPage')->name('editAccountPage');
    Route::get('account/{account}/pin', 'AccountController@editAccountPinPage')->name('editAccountPinPage');

    Route::get('deposit','DepositController@accountPage')->name('depositPage');

    Route::get('withdraw','WithdrawController@accountPage')->name('withdrawPage');

    Route::get('transfer','TransferController@accountPage')->name('transferPage');

    Route::get('logout','LoginController@logout')->name('logout');
});
